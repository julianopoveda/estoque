<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Status;
use app\models\StatusSearch;

class StatusController extends Controller{
    
    public function actionIndex()
    {
        $searchModel = new StatusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCreate()
    {
        $model = new Status();
        
        if($model->load(Yii::$app->request->post()) && $model->save())
        {
            //Se consegui lançar rediciona para o index
            return $this->redirect(['index']);
        }
        else
        {
            return $this->render('create', ['model' => $model]);
        }
    }
}