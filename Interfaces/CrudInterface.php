<?php
namespace app\interfaces;

Interface Crud
{
    public function RetrieveAll();
    public function RetrieveByID($id);
    public function Insert($model);
    public function Update($model);
    public function Delete($id);
}