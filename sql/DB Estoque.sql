CREATE DATABASE Estoque
use Estoque

CREATE TABLE Produto
(
	ID int primary key AUTO_INCREMENT not null,
	Descricao varchar(255) not null,
	Valor_Compra decimal(10,2),
	Valor_Restoqueevenda decimal(10,2) not null 
),

/*CREATE TABLE Embalagem
(
	ID int primary key AUTO_INCREMENT not null,
	ID_Produto int not null,
	Quantidade_Produto int not null,
	Quantidade_Produto_Alerta int not null,
	Valor_Total_Revenda decimal(10,2) not null,
	Valor_Total_Compra decimal(10,2) not null,
	Valor_Unitario_Compra
	
	foreign key ID_Produto references(Produto)
),*/


CREATE TABLE Estoque
(
	ID int primary key AUTO_INCREMENT not null,
	ID_Produto int not null,
	Quantidade_Produto int not null,/*Vai sendo reduzido ou aumentando pela movimentação*/
	Quantidade_Produto_Alerta int not null,
	
	foreign key (ID_Produto) references Produto(ID)
)

CREATE TABLE Movimentacao
(
	ID int primary key AUTO_INCREMENT not null,
	ES bit not null,/*E: 0 e S: 1*/
	Qtd_Movimentada int not null,/*Quantidade de Cápsulas a entrar e/ou sair*/
	Data_Movimentacao datetime not null,
	ID_Status int,
	Obs_Movimentacao varchar(512),
	
	foreign key (ID_Status) references Status(ID)
),

--Tabelas de Apoio
--Status no Momento: Pago, Parcial, Não Pago
CREATE TABLE Status
(
	ID int primary key AUTO_INCREMENT not null,
	Status_Desc varchar(255) not null
)