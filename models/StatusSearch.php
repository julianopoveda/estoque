<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class StatusSearch extends Status
{
    //Regras de validação
    public function rules()
    {
        return [
            [['ID'], 'integer'],
            [['Status_Desc'], 'safe'],
        ];
    }
    
    
    public function search($params)
    {
        $query = Status::find();
        
        $dataProvider = new ActiveDataProvider([ 'query'=>$query, ]);
        
        $this->load($params);
        
        if(!$this->validate())
        {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Status_Desc' => $this->Status_Desc
        ]);
        
        return $dataProvider;
    }
}