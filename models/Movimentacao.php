<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "movimentacao".
 *
 * @property integer $ID
 * @property boolean $ES
 * @property integer $Qtd_Movimentada
 * @property string $Data_Movimentacao
 * @property integer $ID_Status
 * @property string $Obs_Movimentacao
 *
 * @property Status $iDStatus
 */
class Movimentacao extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movimentacao';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ES'], 'boolean'],
            [['Qtd_Movimentada', 'Data_Movimentacao'], 'required'],
            [['Qtd_Movimentada', 'ID_Status'], 'integer'],
            [['Data_Movimentacao'], 'safe'],
            [['Obs_Movimentacao'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ES' => 'Es',
            'Qtd_Movimentada' => 'Qtd  Movimentada',
            'Data_Movimentacao' => 'Data  Movimentacao',
            'ID_Status' => 'Id  Status',
            'Obs_Movimentacao' => 'Obs  Movimentacao',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDStatus()
    {
        return $this->hasOne(Status::className(), ['ID' => 'ID_Status']);
    }
}
