<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\interfaces;//pode dar erro aqui

/**
 * ContactForm is the model behind the contact form.
 */
class Status extends \yii\db\ActiveRecord //implements Crud
{
    //public $ID;
    //public $Status_Desc;
 
    //Definir o nome da Tabela
    public static function tableName()
    {
        return 'status';
    }
    
//    public function __construct($id, $description)
//    {
//        $this->ID = $id;
//        $this->Status_Desc = $description;
//    }

    //Regras de validação
    public function rules()
    {
        return [
            [['Status_Desc'], 'required'],
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Status_Desc' => 'Status',
        ];            
    }
    
    public function RetrieveAll()
    {
        return Status::findAll();
    }

    public function RetrieveByID($id)
    {
        return Status::findOne($id);
        //FindOne não faz a pesquisa somente por 1 registro ele só retorna o primeiro que a lista possi. 
        //Para resolver isso usar find([$id])->limit(1)->one()
    }
    
//    public function Insert($model)
//    {
//        //ver isso
//        $model->save();
//    }
//    
//    public function Update($model)
//    {
//        $status = RetrieveByID($model->id);
//        $status->Status_Desc = $model->Status_Desc;
//        $status->save();
//    }
//    public function Delete($id)
//    {
//        $status_delete = RetrieveByID($id);
//        $status_delete->delete();
//    }
}
