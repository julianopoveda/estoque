<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Movimentacao;

/**
 * MovimentacaoSearch represents the model behind the search form about `app\models\Movimentacao`.
 */
class MovimentacaoSearch extends Movimentacao
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Qtd_Movimentada', 'ID_Status'], 'integer'],
            [['ES'], 'boolean'],
            [['Data_Movimentacao', 'Obs_Movimentacao'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Movimentacao::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ES' => $this->ES,
            'Qtd_Movimentada' => $this->Qtd_Movimentada,
            'Data_Movimentacao' => $this->Data_Movimentacao,
            'ID_Status' => $this->ID_Status,
        ]);

        $query->andFilterWhere(['like', 'Obs_Movimentacao', $this->Obs_Movimentacao]);

        return $dataProvider;
    }
}
