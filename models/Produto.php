<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "produto".
 *
 * @property integer $ID
 * @property string $Descricao
 * @property string $Valor_Compra
 * @property string $Valor_Revenda
 *
 * @property Estoque[] $estoques
 */
class Produto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'produto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Descricao', 'Valor_Revenda'], 'required'],
            [['Valor_Compra', 'Valor_Revenda'], 'number'],
            [['Descricao'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Descricao' => 'Descricao',
            'Valor_Compra' => 'Valor  Compra',
            'Valor_Revenda' => 'Valor  Revenda',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstoques()
    {
        return $this->hasMany(Estoque::className(), ['ID_Produto' => 'ID']);
    }
}
