<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estoque".
 *
 * @property integer $ID
 * @property integer $ID_Produto
 * @property integer $Quantidade_Produto
 * @property integer $Quantidade_Produto_Alerta
 *
 * @property Produto $iDProduto
 */
class Estoque extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'estoque';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_Produto', 'Quantidade_Produto', 'Quantidade_Produto_Alerta'], 'required'],
            [['ID_Produto', 'Quantidade_Produto', 'Quantidade_Produto_Alerta'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ID_Produto' => 'Id  Produto',
            'Quantidade_Produto' => 'Quantidade  Produto',
            'Quantidade_Produto_Alerta' => 'Quantidade  Produto  Alerta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDProduto()
    {
        return $this->hasOne(Produto::className(), ['ID' => 'ID_Produto']);
    }
}
