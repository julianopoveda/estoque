<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="status-form">

    <?php $form = ActiveForm :: begin(); ?>
    
    <?= $form->field($model, 'Status_Desc')->textInput() ?>
    
    <div class="form-group">
<!--        Aqui verifica se o form esta sendo chamado da tela de create ou update-->
     <?= Html :: submitButton($model->isNewRecord? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    
</div>