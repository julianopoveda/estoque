<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Movimentacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="movimentacao-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ES')->checkbox() ?>

    <?= $form->field($model, 'Qtd_Movimentada')->textInput() ?>

    <?= $form->field($model, 'Data_Movimentacao')->textInput() ?>

    <?= $form->field($model, 'ID_Status')->textInput() ?>

    <?= $form->field($model, 'Obs_Movimentacao')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
