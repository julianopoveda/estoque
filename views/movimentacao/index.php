<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MovimentacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Movimentacaos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="movimentacao-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Movimentacao', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'ES:boolean',
            'Qtd_Movimentada',
            'Data_Movimentacao',
            'ID_Status',
            // 'Obs_Movimentacao',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
