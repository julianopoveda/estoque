<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Estoque */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estoque-form">

    <?php $form = ActiveForm::begin(); ?>

<!--    por enquanto esse não funciona-->
    <?= $form->field($model, 'ID_Produto')->dropdownlist($productsList, ['prompt'=>'Selecione...']) ?>

    <?= $form->field($model, 'Quantidade_Produto')->textInput() ?>

    <?= $form->field($model, 'Quantidade_Produto_Alerta')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
